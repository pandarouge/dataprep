package org.dataprep

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


object DataPrep {

  def main(args: Array[String]) = {

    val csvPath = args(0)

    val sparkSession = SparkSession.builder
      .appName("DataPrep")
      .master(s"local")
      .getOrCreate()

    var csvDF = sparkSession
      .read
      .option("delimiter", ";")
      .option("header", "true")
      .csv(csvPath)

    /* Subset of operators to implement (* if done)
    replace string in column *
    concat two columns *
    split column in multiple columns starting by prefix col_
    split column in multiple rows *
    Extract date elements
    Filter rows/cells on value *
    Filter rows/cells on numerical range *
    Remove column *
    Flag rows on value *
     */

    object OpKind extends Enumeration {
      val Process, Filter, DropColumn = Value
    }

    case class Op(kind: OpKind.Value, column: String, expr: String)

    val operations = List(
      // replace string inplace
      Op(OpKind.Process, "Car", """regexp_replace(Car, " ", "_")"""),
      // build column from two columns (here numerical op), same for concat etc
      Op(OpKind.Process, "MPGCYL", "MPG * Cylinders"),
      // split column into multiple rows
      Op(OpKind.Process, "Car", """explode(split(Car, "_"))"""),
      // Filter rows/cells on value (with string pattern)
      Op(OpKind.Filter, "Car", """Car LIKE "C%" """),
      // Filter rows/cells on numerical range
      Op(OpKind.Filter, "Horsepower", """Horsepower BETWEEN 150 AND 200"""),
      // Flag rows on value
      Op(OpKind.Process, "Flag", """case when MPGCYL = 0.0 then False else True end """),
      // remove column
      Op(OpKind.DropColumn, "Cylinders", "")
    )

    csvDF.show(10)

    csvDF = operations
        .foldLeft(csvDF)((df, op) => df.transform {
          op.kind match {
            case OpKind.Process => _.withColumn(op.column, expr(op.expr))
            case OpKind.Filter => _.filter(op.expr)
            case OpKind.DropColumn => _.drop(op.column)
          }
        })

    csvDF.show(10)

    sparkSession.close()
  }
}
