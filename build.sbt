name := "DataPrep"

version := "1.0"

scalaVersion := "2.12.8"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.4"

libraryDependencies ++= Seq("org.slf4j" % "slf4j-api" % "1.7.5")

libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0" % "test"

fork in run := true